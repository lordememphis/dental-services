import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthGuard } from './auth/auth.guard';

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    RouterModule.forRoot([
      {
        path: 'auth',
        loadChildren: async () =>
          (await import('./auth/auth.module')).AuthModule,
        pathMatch: 'full',
      },
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        loadChildren: async () =>
          (await import('./home/home.module')).HomeModule,
      },
      {
        path: 'appointments',
        loadChildren: async () =>
          (await import('./appointments/appointments.module'))
            .AppointmentsModule,
        canActivate: [AuthGuard],
      },
      {
        path: 'medication',
        loadChildren: async () =>
          (await import('./products/products.module')).ProductsModule,
        canActivate: [AuthGuard],
      },
      {
        path: 'near-me',
        loadChildren: async () =>
          (await import('./outlets/outlets.module')).OutletsModule,
        canActivate: [AuthGuard],
      },
    ]),
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
