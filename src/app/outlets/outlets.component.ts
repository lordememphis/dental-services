import { Component } from '@angular/core';
import { ApiService } from '../data';

@Component({
  selector: 'app-outlets',
  templateUrl: './outlets.component.html',
})
export class OutletsComponent {
  outlets = this.api.outlets;
  constructor(private readonly api: ApiService) {}
}
