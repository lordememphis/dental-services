import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OutletsComponent } from './outlets.component';
import { RouterModule } from '@angular/router';

@NgModule({
  declarations: [OutletsComponent],
  imports: [
    CommonModule,
    RouterModule.forChild([{ path: '', component: OutletsComponent }]),
  ],
})
export class OutletsModule {}
