export interface Outlet {
  address: string;
  name: string;
  thumbnailUrl: string;
}
