export interface Appointment {
  patientId: string;
  appointments: AppointmentInfo[];
}

export interface AppointmentInfo {
  scheduleDate: Date;
  description: string;
}
