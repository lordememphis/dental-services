export interface Product {
  description: string;
  name: string;
  price: number;
  thumbnailUrl: string;
}
