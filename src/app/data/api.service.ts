import { Injectable } from '@angular/core';
import outletData from 'src/assets/data/outlets.data.json';
import productData from 'src/assets/data/products.data.json';
import { Appointment, Outlet, Product } from '.';

@Injectable({ providedIn: 'root' })
export class ApiService {
  get products(): Product[] {
    return productData;
  }

  get outlets(): Outlet[] {
    return outletData;
  }

  getUserAppointments(patientId: string): Appointment {
    const apoointments = JSON.parse(
      localStorage.getItem('appointments')!
    ) as Appointment[];
    return apoointments.find(
      (appointment) => appointment.patientId === patientId
    ) as Appointment;
  }

  bookAppointment(
    patientId: string,
    description: string,
    scheduleDate: Date
  ): void {
    let appointments: Appointment[] = [];

    if (localStorage.getItem('appointments')) {
      appointments = JSON.parse(
        localStorage.getItem('appointments')!
      ) as Appointment[];

      const userAppointments = appointments.find(
        (appointment) => appointment.patientId === patientId
      ) as Appointment;

      if (userAppointments) {
        userAppointments.appointments.push({ description, scheduleDate });
      } else {
        appointments.push({
          patientId,
          appointments: [{ description, scheduleDate }],
        });
      }
    } else {
      appointments.push({
        patientId,
        appointments: [{ description, scheduleDate }],
      });
    }

    localStorage.setItem('appointments', JSON.stringify(appointments));
  }
}
