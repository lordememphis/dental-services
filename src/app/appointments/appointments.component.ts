import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ApiService, AppointmentInfo } from '../data';

@Component({
  selector: 'app-appointments',
  templateUrl: './appointments.component.html',
})
export class AppointmentsComponent implements OnInit {
  appointments!: AppointmentInfo[];
  form!: FormGroup;
  uuid = localStorage.getItem('uuid')!;

  constructor(
    private readonly api: ApiService,
    private readonly router: Router
  ) {}

  ngOnInit(): void {
    this.form = new FormGroup({
      description: new FormControl(null, Validators.required),
      scheduleDate: new FormControl(null, Validators.required),
    });

    this.appointments = this.api.getUserAppointments(this.uuid)?.appointments;
  }

  bookAppointment(): void {
    if (this.form.valid) {
      this.api.bookAppointment(
        this.uuid,
        this.form.get('description')?.value,
        this.form.get('scheduleDate')?.value
      );

      this.router
        .navigateByUrl('/medication')
        .then(() => this.router.navigate(['/']));
    }
  }
}
