import { Component } from '@angular/core';
import { ApiService, Product } from '../data';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
})
export class ProductsComponent {
  products = this.api.products;
  collection: Product[] = [];

  constructor(private readonly api: ApiService) {}

  addProduct(product: Product): void {
    this.collection.push(product);
  }

  checkout(): void {
    if (this.collection.length) {
      alert(
        `You order has been processed successfully, for ${this.collection.length} items!`
      );
      this.collection = [];
    } else {
      alert('Buy items to checkout!');
    }
  }
}
